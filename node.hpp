#ifndef NODE_HPP
#define NODE_HPP

template <typename T>
class List;

template <typename T>
class Node
{

 private:
  T elem;
  Node<T>* next;
  friend class List<T>;

};

#endif
