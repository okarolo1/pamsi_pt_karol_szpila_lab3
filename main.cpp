#include <iostream>
#include "stack_list.hpp"
#include <cstdlib>
using namespace std;

void hanoi(int n, StackList<int>& A, StackList<int>& B, StackList<int>& C)
{
  if (n > 0)
  {
    hanoi(n-1, A, C, B);
    int elem = A.Pop();
    C.Push(elem);
    std::cout << "A" << std::endl;
    A.Print();
    std::cout << "\nB" << std::endl;
    B.Print();
    std::cout << "\nC" << std::endl;
    C.Print();
    char pause;
    std::cin >> pause;
    system("clear");
    hanoi(n-1, B, A, C);
  }
}

int main()
{
  StackList<int> A,B,C;
  int wysokosc;
  std::cout << "Podaj wysokosc wierzy" << std::endl;
  std::cin >> wysokosc;
  system("clear");
  for(int i = wysokosc; i>0;i--)
	A.Push(i);

  hanoi(wysokosc, A, B, C);
  return 0;
}
