#ifndef LIST_HPP
#define LIST_HPP

#include <iostream>
#include "node.hpp"
#include <string>
#define nullptr 0

class EmptyListException{};

template <typename T> 
class List
{

 public:
  List();
  ~List();
  void AddFront(const T&);
  T RemoveFront()throw(EmptyListException);
  void Print() const;
  void AddBack(const T& e);
  void Clear();
  int Length() const { return length;};

 private:
  Node<T>* head;
  Node<T>* tail;
  int length;
};


template <typename T>
List<T>::List() : head(nullptr), length(0)
{}

template <typename T>
List<T>::~List()
{
  Clear();
}


template <typename T>
void List<T>::Clear()
{
  while(head != nullptr)
    {
      Node<T>* tmp = head;
      head = head->next;
      delete tmp;
    }
  head = nullptr;
}


template <typename T>
void List<T>::Print() const
{
  Node<T>* tmp = head;
  while(tmp != nullptr)
    {
      std::cout << tmp->elem << std::endl;     
      tmp = tmp->next;
      
    }
}


template <typename T>
void List<T>::AddFront(const T& e)
{
  Node<T>* tmp = new Node<T>;
  tmp->elem = e;
  tmp->next = head;
  head = tmp;
  if(length == 0)
    tail = head;
  length++;
}


template <typename T>
void List<T>::AddBack(const T& e)
{
  Node<T>* new_elem = new Node<T>;
  new_elem->elem = e;
  new_elem->next = nullptr;

  if(head == nullptr){
    head = new_elem;
    tail = head;
    return;
  }

  tail->next = new_elem;
  tail = new_elem;
  length++;
}


template <typename T>
T List<T>::RemoveFront() throw(EmptyListException)
{
  if(head == nullptr)
    throw EmptyListException();
  Node<T>* tmp = head;
  T value = tmp->elem;
  head = head->next;
  delete tmp;
  length--;
  return value;
}

#endif
