#ifndef STACK_HPP
#define STACK_HPP

#define NULL 0
#define C capacity

#include <iostream>

class EmptyStackException{};

template<typename T>
class StackTab
{
public:
  StackTab();
  ~StackTab(){delete[] tab;};

  void Push(T elem);
  T Pop() throw(EmptyStackException);
  int Size() const {return top + 1;};
  bool isEmpty() const;

  void Print() const;
  void Clear();
  
private:
  void Resize_Stack();

  T* tab;
  int capacity;
  int top;
};

template<typename T>
StackTab<T>::StackTab() : tab(NULL), capacity(1), top(-1)
{
  tab = new T[capacity];
}

template<typename T>
bool StackTab<T>::isEmpty() const
{
  if(top == -1)
    return true;
  return false;
}

template<typename T>
void StackTab<T>::Resize_Stack()
{
  T* new_tab = new T[capacity*2];
  if(tab != NULL)
    {
      for(int i = 0; i < top; i++)
	new_tab[i] = tab[i];
      delete[] tab;
    }
  tab = new_tab;
  capacity += C;
}

template<typename T>
void StackTab<T>::Push(T elem)
{
  top++;
  if( top >= capacity)
    Resize_Stack();
  tab[top] = elem;
}

template<typename T>
T StackTab<T>::Pop() throw(EmptyStackException)
{
  T ret_val = tab[top];
  --top;
  if(top < -1)
    throw EmptyStackException();
  return ret_val;
}

template<typename T>
void StackTab<T>::Print() const
{
  for(int i = 0; i <= top; i++)
    std::cout << tab[i] << std::endl;

}

template<typename T>
void StackTab<T>::Clear()
{
  top = -1;
  capacity = 0;
  delete[] tab;
  tab = NULL;
}

#endif
