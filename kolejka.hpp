#ifndef KOLEJKA_HPP
#define KOLEJKA_HPP

#include "list.hpp"
#include <string>

class EmptyQueueException{};

template <typename T>
class Kolejka
{
public:
  void Enqueue(const T& e);
  T Dequeue() throw(EmptyQueueException);
  void Print() const;
  void Clear();

  int Size() const{return lista.Length();};
  bool isEmpty() const;
  
private:
  List<T> lista;

};

template <typename T>
bool Kolejka<T>::isEmpty() const
{
  if(lista.Length() == 0)
    return true;
  return false;
}

template <typename T>
void Kolejka<T>::Enqueue(const T& e)
{
  lista.AddBack(e);
}

template <typename T>
T Kolejka<T>::Dequeue() throw(EmptyQueueException)
{
  try
    {
      T elem = lista.RemoveFront();
      return elem;
    }
  catch(EmptyListException err)
    {
      throw EmptyQueueException();
    }
  
}

template <typename T>
void Kolejka<T>::Print() const
{
  lista.Print();
}

template <typename T>
void Kolejka<T>::Clear()
{
  lista.Clear();
}


#endif
