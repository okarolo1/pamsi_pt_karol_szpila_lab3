#ifndef STACK_LIST
#define STACK_LIST

#include "list.hpp"

class EmptyStackException{};

template<typename T>
class StackList
{
public:
  void Push(T elem);
  T Pop() throw(EmptyStackException);
  void Print() const;
  int Size() const {return list.Length();};
  bool isEmpty() const;
private:
  List<T> list;
  
};

template<typename T>
bool StackList<T>::isEmpty() const
{
  if(list.Length() == 0)
    return true;
  return false;
}
template<typename T>
void StackList<T>::Push(T elem)
{
  list.AddFront(elem);
}


template<typename T>
T StackList<T>::Pop() throw(EmptyStackException)
{
  try
    {
      return list.RemoveFront();
    }
  catch(EmptyListException err)
    {
      throw EmptyStackException();
    }
}

template<typename T>
void StackList<T>::Print() const
{
  list.Print();
}


#endif
 
