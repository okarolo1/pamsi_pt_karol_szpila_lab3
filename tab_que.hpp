#ifndef TAB_QUE
#define TAB_QUE

#define NULL 0
#define C capacity

#include <iostream>

class EmptyQueueException{};

template<typename T>
class TabQueue
{
public:
  TabQueue(); 
  ~TabQueue() { delete[] tab;};
  
  void Enqueue(T elem);
  T Dequeue() throw(EmptyQueueException);

  void Print() const;
  int Size() const{return end - begin;};
  bool Empty() const;
private:
  void Resize();

  T* tab;
  int capacity;
  int begin;
  int end;

};

template<typename T>
TabQueue<T>::TabQueue() : tab(NULL),capacity(0),begin(0),end(0)
{
  tab = new T[2];
  capacity = 2;
}

template<typename T>
void TabQueue<T>::Resize()
{
  T* new_tab = new T[capacity + C];
  for(int i = 0; i < capacity; i++)
    {
      int index = i+begin;
      if(index >= capacity)
	index-=capacity;
      new_tab[i] = tab[index];
    }
  begin = 0;
  end = capacity;
  capacity += C;
  delete[] tab;
  tab = new_tab;
}


template<typename T>
void TabQueue<T>::Enqueue(T elem)
{
  tab[end] = elem;
  end++;
  if(end >= capacity)
    {
      end = 0;
    }
  if(end == begin)
    Resize();
}

template<typename T>
T TabQueue<T>::Dequeue() throw(EmptyQueueException)
{
  if(end == begin)
    throw EmptyQueueException();
  end--;
  if(end < 0)
    end += capacity;
  return tab[end];
}


template<typename T>
void TabQueue<T>::Print() const
{
  int index = begin;
  while(index != end)
    {
      std::cout << tab[index] << std::endl;;
      if( ++index >= capacity)
	index = 0;
    }

}

template<typename T>
bool TabQueue<T>::Empty() const
{
  if( Size() == 0 )
    return true;
  return false;
}
#endif 
